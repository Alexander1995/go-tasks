package main

import (
	"fmt"
	"runtime"
)

func main() {
	if err := testPanic(); err != nil {
		fmt.Println("Error:", err)
	}
}

func testPanic() (err error) {
	var ok bool
	var r interface{}
	defer func() {
		if r := recover(); r != nil {
			if err, ok = r.(error); ok {
				fmt.Println()
			}
		}
		fmt.Println("PANIC Deferred")
		//Capture the stack trace
		buf := make([]byte, 10000)
		runtime.Stack(buf, false)
		fmt.Println("Stack trace: ", string(buf))

		if err != nil {
			err = fmt.Errorf("%v", r)
		}
	}()

	fmt.Println("Start Test")

	panic("at the disco")
	err = mimicError("1")

	var p *int
	*p = 10

	fmt.Println("End test")
	return err
}

func mimicError(key string) (err error) {
	return fmt.Errorf("Mimic Error %v", key)
}
