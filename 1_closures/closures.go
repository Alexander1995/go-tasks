package main

import (
	"fmt"
	"time"
)

func main() {
	myTimer := getTimer()
	defer myTimer()

	f := func() {
		myTimer()
	}

	f()
}

func withClosure() func() {
	return func() {
		fmt.Println("That text goes from closure")
	}
}

func getTimer() func() {
	start := time.Now()
	return func() {
		fmt.Printf("The time from start is %v\n", time.Since(start))
	}
}
